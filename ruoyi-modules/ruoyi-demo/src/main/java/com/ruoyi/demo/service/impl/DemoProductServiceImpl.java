package com.ruoyi.demo.service.impl;

import java.util.List;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import com.ruoyi.demo.mapper.DemoProductMapper;
import com.ruoyi.demo.domain.DemoProduct;
import com.ruoyi.demo.service.IDemoProductService;

/**
 * 产品树表（mb）Service业务层处理
 * 
 * @author 数据小王子
 * 2023-07-11
 */
@Service
public class DemoProductServiceImpl implements IDemoProductService 
{
    @Resource
    private DemoProductMapper demoProductMapper;

    /**
     * 查询产品树表（mb）
     * 
     * @param productId 产品树表（mb）主键
     * @return 产品树表（mb）
     */
    @Override
    public DemoProduct selectDemoProductByProductId(Long productId)
    {
        return demoProductMapper.selectDemoProductByProductId(productId);
    }

    /**
     * 查询产品树表（mb）列表
     * 
     * @param demoProduct 产品树表（mb）
     * @return 产品树表（mb）
     */
    @Override
    public List<DemoProduct> selectDemoProductList(DemoProduct demoProduct)
    {
        return demoProductMapper.selectDemoProductList(demoProduct);
    }

    /**
     * 新增产品树表（mb）
     * 
     * @param demoProduct 产品树表（mb）
     * @return 结果
     */
    @Override
    public int insertDemoProduct(DemoProduct demoProduct)
    {
        return demoProductMapper.insertDemoProduct(demoProduct);
    }

    /**
     * 修改产品树表（mb）
     * 
     * @param demoProduct 产品树表（mb）
     * @return 结果
     */
    @Override
    public int updateDemoProduct(DemoProduct demoProduct)
    {
        return demoProductMapper.updateDemoProduct(demoProduct);
    }

    /**
     * 批量删除产品树表（mb）
     * 
     * @param productIds 需要删除的产品树表（mb）主键
     * @return 结果
     */
    @Override
    public int deleteDemoProductByProductIds(Long[] productIds)
    {
        return demoProductMapper.deleteDemoProductByProductIds(productIds);
    }

    /**
     * 删除产品树表（mb）信息
     * 
     * @param productId 产品树表（mb）主键
     * @return 结果
     */
    @Override
    public int deleteDemoProductByProductId(Long productId)
    {
        return demoProductMapper.deleteDemoProductByProductId(productId);
    }
}
