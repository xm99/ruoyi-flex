package com.ruoyi.demo.mapper;

import java.util.List;
import com.ruoyi.demo.domain.DemoCustomer;
import com.ruoyi.demo.domain.DemoGoods;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户主表(mb)Mapper接口
 *
 * @author 数据小王子
 * 2023-07-11
 */
@Mapper
public interface DemoCustomerMapper
{
    /**
     * 查询客户主表(mb)
     *
     * @param customerId 客户主表(mb)主键
     * @return 客户主表(mb)
     */
    DemoCustomer selectDemoCustomerByCustomerId(Long customerId);

    /**
     * 查询客户主表(mb)列表
     *
     * @param demoCustomer 客户主表(mb)
     * @return 客户主表(mb)集合
     */
    List<DemoCustomer> selectDemoCustomerList(DemoCustomer demoCustomer);

    /**
     * 新增客户主表(mb)
     *
     * @param demoCustomer 客户主表(mb)
     * @return 结果
     */
    int insertDemoCustomer(DemoCustomer demoCustomer);

    /**
     * 修改客户主表(mb)
     *
     * @param demoCustomer 客户主表(mb)
     * @return 结果
     */
    int updateDemoCustomer(DemoCustomer demoCustomer);

    /**
     * 删除客户主表(mb)
     *
     * @param customerId 客户主表(mb)主键
     * @return 结果
     */
    int deleteDemoCustomerByCustomerId(Long customerId);

    /**
     * 批量删除客户主表(mb)
     *
     * @param customerIds 需要删除的数据主键集合
     * @return 结果
     */
    int deleteDemoCustomerByCustomerIds(Long[] customerIds);

    /**
     * 批量删除商品子
     *
     * @param customerIds 需要删除的数据主键集合
     * @return 结果
     */
    int deleteDemoGoodsByCustomerIds(Long[] customerIds);

    /**
     * 批量新增商品子
     *
     * @param demoGoodsList 商品子列表
     * @return 结果
     */
    int batchDemoGoods(List<DemoGoods> demoGoodsList);


    /**
     * 通过客户主表(mb)主键删除商品子信息
     *
     * @param customerId 客户主表(mb)ID
     * @return 结果
     */
    int deleteDemoGoodsByCustomerId(Long customerId);
}
