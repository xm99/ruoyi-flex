package com.ruoyi.mf.mapper;

import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.ruoyi.mf.domain.MfStudent;

/**
 * 学生信息表Mapper接口
 *
 * @author 数据小王子
 * 2023-11-22
 */
@Mapper
public interface MfStudentMapper extends BaseMapper<MfStudent>
{

}
